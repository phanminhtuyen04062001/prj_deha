<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class GetListProductTest extends TestCase
{
    public function getRoute()
    {
        return route('products.index');
    }

    /** @test */
    public function super_admin_can_get_all_products()
    {
        $this->loginAsSuperAdmin();
        $response = $this->get($this->getRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.products.index');
        $response->assertViewHas('products');
    }

    /** @test */
    public function authenticated_user_without_permission_can_not_get_all_products()
    {
        $this->loginAsUser();
        $response = $this->get($this->getRoute());
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticated_user_has_permission_can_get_all_products()
    {
        $this->loginAsUserWithPermission('products-view');
        $response = $this->get($this->getRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.products.index');
        $response->assertViewHas('products');
    }

    /** @test  */
    public function unauthenticated_user_can_not_get_all_products()
    {
        $response = $this->get($this->getRoute());
        $response->assertRedirect('/login');
        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test */
    public function authenticated_user_can_get_list_products()
    {
        $this->loginAsUserWithPermission('products-view');
        $response = $this->get(route('products.table'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.products.list');
    }
}
