<?php

namespace Tests\Feature\Product;

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class CreateProductTest extends TestCase
{
    public function getRoute()
    {
        return route('products.store');
    }

    public function setDataCreate($data = [])
    {
        return array_merge([
            'name' => $this->faker->unique->name,
            'price' => '100000',
            'describe' => $this->faker->text,
            'category_ids' => Category::inRandomOrder()->first()->id,
        ], $data);
    }

    /** @test */
    public function authenticated_super_admin_can_create_new_product()
    {
        $this->loginAsSuperAdmin();
        $data = $this->setDataCreate();
        $response = $this->postJson($this->getRoute(), $data);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(function (AssertableJson $json){
            $json->where('status', Response::HTTP_OK)
                ->where('message', 'Save success')
                ->etc();
        });
    }

    /** @test  */
    public function authenticated_and_user_has_permission_user_can_create_new_product()
    {
        $this->loginAsUserWithPermission('products-store');
        $data = $this->setDataCreate();
        $response = $this->postJson($this->getRoute(), $data);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(function (AssertableJson $json){
            $json->where('status', Response::HTTP_OK)
                ->where('message', 'Save success')
                ->etc();
        });
    }

    /** @test  */
    public function unauthenticated_user_can_not_create_new_product()
    {
        $data = $this->setDataCreate();
        $response = $this->postJson($this->getRoute(), $data);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    /** @test  */
    public function user_can_not_create_new_product_if_name_field_is_null()
    {
        $this->loginAsUserWithPermission('products-store');
        $data = $this->setDataCreate(['name' => '']);
        $response = $this->postJson($this->getRoute(), $data);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(function (AssertableJson $json){
            $json->has('errors')
                ->where('message', 'The given data was invalid.')
                ->etc();
        });
    }

    /** @test  */
    public function super_admin_can_not_create_new_product_if_name_field_is_null()
    {
        $this->loginAsSuperAdmin();
        $data = $this->setDataCreate(['name' => '']);
        $response = $this->postJson($this->getRoute(), $data);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(function (AssertableJson $json){
            $json->has('errors')
                ->where('message', 'The given data was invalid.')
                ->etc();
        });
    }
}
