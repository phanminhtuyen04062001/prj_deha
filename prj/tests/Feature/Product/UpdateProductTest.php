<?php

namespace Tests\Feature\Product;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class UpdateProductTest extends TestCase
{
    public function getRoute($id)
    {
        return route('products.update', $id);
    }

    public function setDataUpdate($data = [])
    {
        return array_merge([
            'name' => $this->faker->unique->name,
            'price' => '2000000',
            'describe' => $this->faker->text,
            'category_ids' => Category::inRandomOrder()->first()->id,
        ], $data);
    }

    /** @test  */
    public function authenticated_user_has_permission_can_update_product()
    {
        $this->loginAsUserWithPermission('products-update');
        $product = Product::inRandomOrder()->first();
        $data = $this->setDataUpdate();
        $response = $this->patchJson($this->getRoute($product->id), $data);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(function (AssertableJson $json){
            $json->where('status', Response::HTTP_OK)
                ->where('message', 'Update success')
                ->etc();
        });
    }

    /** @test  */
    public function authenticated_super_admin_can_update_product()
    {
        $this->loginAsSuperAdmin();
        $product = Product::inRandomOrder()->first();
        $data = $this->setDataUpdate();
        $response = $this->patchJson($this->getRoute($product->id), $data);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(function (AssertableJson $json){
            $json->where('status', Response::HTTP_OK)
                ->where('message', 'Update success')
                ->etc();
        });
    }

    /** @test  */
    public function unauthenticated_user_can_not_update_product()
    {
        $product = Product::inRandomOrder()->first();
        $data = $this->setDataUpdate();
        $response = $this->patchJson($this->getRoute($product->id), $data);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    /** @test  */
    public function unauthenticated_super_admin_can_not_update_product()
    {
        $product = Product::inRandomOrder()->first();
        $data = $this->setDataUpdate();
        $response = $this->patchJson($this->getRoute($product->id), $data);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    /** @test  */
    public function user_has_permission_can_not_update_product_if_name_is_null()
    {
        $this->loginAsUserWithPermission('products-update');
        $product = Product::inRandomOrder()->first();
        $data = $this->setDataUpdate(['name' => '']);
        $response = $this->patchJson($this->getRoute($product->id), $data);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(function (AssertableJson $json){
            $json->has('errors')
              //  ->where('message', 'The given data was invalid.')
                ->etc();
        });
    }

    /** @test  */
    public function super_admin_can_not_update_product_if_name_is_null()
    {
        $this->loginAsSuperAdmin();
        $product = Product::inRandomOrder()->first();
        $data = $this->setDataUpdate(['name' => '']);
        $response = $this->patchJson($this->getRoute($product->id), $data);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(function (AssertableJson $json){
            $json->has('errors')
             //   ->where('message', 'The given data was invalid.')
                ->etc();
        });
    }

}
