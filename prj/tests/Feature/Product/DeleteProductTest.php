<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class DeleteProductTest extends TestCase
{
    public function getRoute($id)
    {
        return route('products.delete', $id);
    }

    /** @test  */
    public function authenticated_and_user_has_permission_user_can_delete_product()
    {
        $this->loginAsUserWithPermission('products-delete');
        $product = Product::factory()->create();
        $response = $this->deleteJson($this->getRoute($product->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(function (AssertableJson $json){
            $json->where('status', Response::HTTP_OK)
                ->where('message', 'Delete success')
                ->etc();
        });
    }

    /** @test */
    public function authenticated_super_admin_can_delete_product()
    {
        $this->loginAsSuperAdmin();
        $product = Product::factory()->create();
        $response = $this->deleteJson($this->getRoute($product->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(function (AssertableJson $json){
            $json->where('status', Response::HTTP_OK)
                ->where('message', 'Delete success')
                ->etc();
        });
    }

    /** @test  */
    public function unauthenticated_user_can_not_delete_delete_product()
    {
        $product = Product::inRandomOrder()->first();
        $response = $this->deleteJson($this->getRoute($product->id));
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    /** @test  */
    public function unauthenticated_super_admin_can_not_delete_product()
    {
        $product = Product::inRandomOrder()->first();
        $response = $this->deleteJson($this->getRoute($product->id));
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

}
