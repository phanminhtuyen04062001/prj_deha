<?php

namespace Tests\Feature\Role;

use App\Models\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListRoleTest extends TestCase
{
    public function getRoute()
    {
        return route('roles.index');
    }
    /** @test */
    public function authenticated_super_admin_can_get_all_roles()
    {
        $this->loginAsSuperAdmin();
        $role = Role::factory()->create();
        $response = $this->get($this->getRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.roles.index');
        $response->assertSee($role->id);
    }

    /** @test  */
    public function authenticated_user_has_permission_can_get_all_roles()
    {
        $this->loginAsUserWithPermission('roles-view');
        $role = Role::factory()->create();
        $response = $this->get($this->getRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.roles.index');
        $response->assertSee($role->name);
    }

    /** @test  */
    public function unauthenticated_user_can_not_get_all_roles()
    {
        $role = Role::factory()->create();
        $response = $this->get($this->getRoute());
        $response->assertRedirect('/login');
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertDontSee($role->display_name);
    }


}
