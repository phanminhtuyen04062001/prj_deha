<?php

namespace Tests\Feature\Role;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class RoleUpdateTest extends TestCase
{
    use WithFaker;

    public function getEditRoute($id)
    {
        return route('roles.edit', $id);
    }

    public function getUpdateRoute($id)
    {
        return route('roles.update', $id);
    }

    public function getDataRolePost()
    {
        $permission = Permission::factory()->create();
        $dataCreate = [
            'name'          => $this->faker->name,
            'display_name'  => $this->faker->text,
            'permission_ids'=> [$permission->id]
        ];
        return $dataCreate;
    }

    public function getDataRole($dataCreate)
    {
        $role = [
            'name'          => $dataCreate['name'],
            'display_name'  => $dataCreate['display_name']
        ];
        return $role;
    }

    public function getDataRoleNameIsNullPost()
    {
        $permissions = Permission::factory()->create();
        $dataCreate = [
            'name'          => '',
            'display_name'  => $this->faker->text,
            'permission_id'=> [$permissions->id]
        ];
        return $dataCreate;
    }
    /** @test  */
    public function authenticated_super_admin_can_see_edit_role_form()
    {
        $this->loginAsSuperAdmin();
        $role = Role::factory()->create();
        $response = $this->get($this->getEditRoute($role->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.roles.edit');
        $response->assertSee('Roles');
    }

    /** @test */
    public function authenticated_super_admin_can_update_role()
    {
        $this->loginAsSuperAdmin();
        $role = Role::factory()->create();
        $dataUpdate = $this->getDataRolePost();
        $response = $this->put($this->getUpdateRoute($role->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('roles', $this->getDataRole($dataUpdate));
    }

    /** @test  */
    public function authenticated_super_admin_can_not_update_role_if_name_is_null()
    {
        $this->loginAsSuperAdmin();
        $role = Role::factory()->create();
        $dataUpdate = $this->getDataRoleNameIsNullPost();
        $response = $this->put($this->getUpdateRoute($role->id), $dataUpdate);
        $response->assertSessionHasErrors(['name']);
        $this->assertDatabaseMissing('roles', $this->getDataRole($dataUpdate));
    }

    /** @test  */
    public function authenticated_user_has_permission_can_see_edit_form()
    {
        $this->loginAsSuperAdmin();
        $role = Role::factory()->create();
        $response = $this->get($this->getEditRoute($role->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.roles.edit');
        $response->assertSee('Roles');
    }

    /** @test  */
    public function authenticated_user_has_permission_can_update_role()
    {
        $this->loginAsSuperAdmin();
        $role = Role::factory()->create();
        $dataUpdate = $this->getDataRolePost();
        $response = $this->put($this->getUpdateRoute($role->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('roles', $this->getDataRole($dataUpdate));
    }

    /** @test  */
    public function authenticated_user_has_permission_can_not_update_role_if_name_is_null()
    {
        $this->loginAsSuperAdmin();
        $role = Role::factory()->create();
        $dataUpdate = $this->getDataRoleNameIsNullPost();
        $response = $this->put($this->getUpdateRoute($role->id), $dataUpdate);
        $response->assertSessionHasErrors(['name']);
        $this->assertDatabaseMissing('roles', $this->getDataRole($dataUpdate));
    }

    /** @test  */
    public function unauthenticated_user_can_not_see_edit_role_form()
    {
        $role = Role::factory()->create();
        $response = $this->get($this->getEditRoute($role->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

}
