<?php

namespace Tests\Feature\Role;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class RoleDeleteTest extends TestCase
{
    use WithFaker;

    public function getDeleteRoute($id)
    {
        return route('roles.index',$id);
    }
    /** @test */
    public function authenticate_super_admin_can_delete_role()
    {
        $user = User::findOrFail(1);
        $this->actingAs($user);
        $dataRole = Role::factory()->create();
        $countBefore = Role::count();
        $response = $this->delete($this->getDeleteRoute($dataRole->id));
        $countAfter = Role::count();
        $this->assertEquals($countBefore,$countAfter + 1);
        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test */
    public function authenticated_user_has_permission_can_delete_role()
    {

        $role = Role::factory()->create();
        $countBefore = Role::count();
        $response = $this->delete($this->getDeleteRoute($role->id));
        $countAfter = Role::count();
        $this->assertEquals($countBefore,$countAfter + 1);
        $response->assertStatus(Response::HTTP_FOUND);
    }
    /** @test */
    public function authenticated_user_without_permission_can_not_delete_role()
    {

        $role = Role::factory()->create();
        $response = $this->delete($this->getDeleteRoute($role->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function unauthenticated_user_can_not_delete_role()
    {
        $role = Role::factory()->create();
        $response = $this->delete($this->getDeleteRoute($role->id));
        $response->assertStatus(Response::HTTP_FOUND);
    }

}
