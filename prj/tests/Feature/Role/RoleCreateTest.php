<?php

namespace Tests\Feature\Role;

use App\Models\Permission;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class RoleCreateTest extends TestCase
{
    use WithFaker;

    public function getCreateRoute()
    {
        return route('roles.create');
    }

    public function getStoreRoute()
    {
        return route('roles.store');
    }

    public function getRoleRoute()
    {
        return route('roles.index');
    }

    public function getDataRolePost()
    {
        $permission = Permission::factory()->create();
        $dataCreate = [
            'name'          => $this->faker->name,
            'display_name'  => $this->faker->text,
            'permission_id'=> [$permission->id]
        ];
        return $dataCreate;
    }

    public function getDataRoleNameIsNullPost()
    {
        $permission = Permission::factory()->create();
        $dataCreate = [
            'name'          => '',
            'display_name'  => $this->faker->text,
            'permission_id'=> [$permission->id]
        ];
        return $dataCreate;
    }

    public function getDataRole($dataCreate)
    {
        $role = [
            'name'          => $dataCreate['name'],
            'display_name'  => $dataCreate['display_name']
        ];
        return $role;
    }

    /** @test */
    public function authenticated_super_admin_can_see_create_role_form()
    {
        $this->loginAsSuperAdmin();
        $response = $this->get($this->getCreateRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.roles.create');
        $response->assertSee("Roles");
    }

    /** @test */
    public function authenticated_super_admin_can_create_new_role()
    {
        $this->loginAsSuperAdmin();
        $dataCreate = $this->getDataRolePost();
        $response = $this->post($this->getStoreRoute(), $dataCreate);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('roles', $this->getDataRole($dataCreate));
        $response->assertRedirect($this->getRoleRoute());
    }


    /** @test */
    public function authenticated_super_admin_can_not_create_new_role_if_name_is_null()
    {
        $this->loginAsSuperAdmin();
        $dataCreate = $this->getDataRoleNameIsNullPost();
        $response = $this->post($this->getStoreRoute(), $dataCreate);
        $response->assertSessionHasErrors(['name']);
        $this->assertDatabaseMissing('roles', $this->getDataRole($dataCreate));
    }


    /** @test */
    public function authenticated_user_has_permission_can_see_create_role_form()
    {
        $this->loginAsSuperAdmin();
        $response = $this->get($this->getCreateRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.roles.create');
        $response->assertSee('Roles');
    }


    /** @test */
    public function authenticated_user_has_permission_can_create_new_role()
    {
        $this->loginAsSuperAdmin();
        $dataCreate = $this->getDataRolePost();
        $response = $this->post($this->getStoreRoute(), $dataCreate);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('roles', $this->getDataRole($dataCreate));
        $response->assertRedirect($this->getRoleRoute());
    }


    /** @test */
    public function authenticated_user_has_permission_can_not_create_new_role_if_name_is_null()
    {
        $this->loginAsSuperAdmin();
        $dataCreate = $this->getDataRoleNameIsNullPost();
        $response = $this->post($this->getStoreRoute(), $dataCreate);
        $response->assertSessionHasErrors(['name']);
        $this->assertDatabaseMissing('roles', $this->getDataRole($dataCreate));
    }

    /** @test */
    public function unauthenticated_user_can_not_see_create_form()
    {
        $response = $this->get($this->getCreateRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function unauthenticated_user_can_not_create_role()
    {
        $dataCreate = $this->getDataRolePost();
        $response = $this->post($this->getStoreRoute(), $dataCreate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
        $this->assertDatabaseMissing('roles', $this->getDataRole($dataCreate));
    }
}
