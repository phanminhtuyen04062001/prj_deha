<?php

namespace Tests\Feature\User;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class CreateUserTest extends TestCase
{
    //private $faker;

    public function getCreateRoute()
    {
        return route('users.create');
    }

    public function getStoreRoute()
    {
        return route('users.store');
    }

    public function getUserRoute()
    {
        return route('users.index');
    }

    /** @test  */
    public function authenticated_super_admin_can_see_create_user_form()
    {
        $this->loginAsSuperAdmin();
        $response = $this->get($this->getCreateRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.users.create');
    }

    /** @test  */
    public function authenticated_user_without_permission_can_not_see_create_user_form()
    {
        $this->loginAsSuperAdmin();
        $response = $this->get($this->getCreateRoute());
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test  */
    public function authenticated_super_admin_can_create_new_user()
    {
        $this->loginAsSuperAdmin();
        $userCountBefore = User::count();
        $role = Role::factory()->create();
        $dataCreate = [
            'name' => $this->faker->name,
            'email' => $this->faker->unique->email,
            'phone' => $this->faker->unique->phoneNumber,
            'password' => Hash::make('123456'),
            'role_ids' => [$role->id]
        ];
        $response = $this->post($this->getStoreRoute(), $dataCreate);
        $userCountAfter = User::count();
        $this->assertEquals($userCountBefore + 1, $userCountAfter);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getUserRoute());
    }

    /** @test  */
    public function authenticated_super_admin_can_not_create_new_user_if_name_is_null()
    {
        $this->loginAsSuperAdmin();
        $role = Role::factory()->create();
        $dataCreate = [
            'name' => null,
            'email' => $this->faker->unique->email,
            'password' => Hash::make('123456'),
            'role_ids' => $role->id
        ];
        $response = $this->post($this->getStoreRoute(), $dataCreate);
        $response->assertSessionHasErrors('name');
        $this->assertDatabaseMissing('users', ['email' => $dataCreate['email']]);
    }

    /** @test  */
    public function unauthenticated_user_can_not_see_create_new_user_form()
    {
        $response = $this->get($this->getCreateRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test  */
    public function unauthenticated_user_can_not_create_new_user()
    {
        $user = User::factory()->make();
        $response = $this->post($this->getStoreRoute(), $user->toArray());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

}
