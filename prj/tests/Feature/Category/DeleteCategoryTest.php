<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteCategoryTest extends TestCase
{
    public function getRoute($id)
    {
         return route('categories.destroy', $id);
    }

    /** @test  */
    public function authenticated_super_admin_can_delete_category()
    {
        $this->loginAsSuperAdmin();
        $category = Category::factory()->create();
        $countBefore = Category::count();
        $response = $this->delete($this->getRoute($category->id));
        $countCountAfter = Category::count();
        $this->assertEquals($countBefore, $countCountAfter + 1);
        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test  */
    public function authenticated_user_has_permission_can_delete_category()
    {
        $this->loginAsUserWithPermission('categories-delete');
        $category = Category::factory()->create();
        $countBefore = Category::count();
        $response = $this->delete($this->getRoute($category->id));
        $countCountAfter = Category::count();
        $this->assertEquals($countBefore, $countCountAfter + 1);
        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test */
    public function unauthenticated_super_admin_can_not_delete_category()
    {
        $category = Category::factory()->create();
        $response = $this->delete($this->getRoute($category->id));
        $response->assertStatus(Response::HTTP_FOUND);
    }

}
