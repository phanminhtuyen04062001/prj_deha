<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Tests\TestCase;

class UpdateCategoryTest extends TestCase
{
    use WithFaker;

    public function getEditRoute($id)
    {
        return route('categories.edit', $id);
    }

    public function getUpdateRoute($id)
    {
        return route('categories.update', $id);
    }

    public function setDataUpdate($data = [])
    {
        $name = $this->faker->unique->name;
        return array_merge([
            'name' => strtoupper($name),
            'slug' => Str::slug($name)
        ], $data);
    }

    /** @test  */
    public function authenticated_super_admin_can_see_edit_category_form()
    {
        $this->loginAsSuperAdmin();
        $category = Category::factory()->create();
        $response = $this->get($this->getEditRoute($category->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.categories.edit');
        $response->assertSee($category->name);
    }

    /** @test  */
    public function authenticated_user_without_permission_can_not_see_edit_category_form()
    {
        $this->loginAsUser();
        $category = Category::factory()->create();
        $response = $this->get($this->getEditRoute($category->id));
        $response->assertStatus(Response::HTTP_OK);
    }

    /** @test  */
    public function authenticated_user_has_permission_can_see_edit_category_form()
    {
        $this->loginAsUserWithPermission('categories-edit');
        $category = Category::factory()->create();
        $response = $this->get($this->getEditRoute($category->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.categories.edit');
        $response->assertSee($category->name);
    }

    /** @test  */
    public function unauthenticated_user_can_not_see_edit_category_form()
    {
        $category = Category::factory()->create();
        $response = $this->get($this->getEditRoute($category->id));
        $response->assertRedirect('/login');
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertDontSee($category->name);
    }

    /** @test  */
    public function authenticated_super_admin_can_update_category()
    {
        $this->loginAsSuperAdmin();
        $category = Category::factory()->create();
        $data = $this->setDataUpdate();
        $response = $this->put($this->getUpdateRoute($category->id), $data);
        $this->assertDatabaseHas('categories', ['name' => $data['name']]);
        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test  */
    public function authenticated_super_admin_can_not_update_category_name_is_null()
    {
        $this->loginAsSuperAdmin();
        $category = Category::factory()->create();
        $data = $this->setDataUpdate(['name' => '']);
        $response = $this->put($this->getUpdateRoute($category->id), $data);
        $response->assertSessionHasErrors('name');
        $this->assertDatabaseMissing('categories', $data);
    }

    /** @test  */
    public function authenticated_user_has_permission_can_not_update_category_name_is_null()
    {
        $this->loginAsUserWithPermission('categories-update');
        $category = Category::factory()->create();
        $data = $this->setDataUpdate(['name' => null]);
        $response = $this->put($this->getUpdateRoute($category->id), $data);
        $response->assertSessionHasErrors(['name']);
        $this->assertDatabaseMissing('categories', $data);
    }

}
