<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Tests\TestCase;

class CreateCategoryTest extends TestCase
{
    public function getCreateRoute()
    {
        return route('categories.create');
    }

    public function getStoreRoute()
    {
        return route('categories.store');
    }

    public function getCategoryRoute()
    {
        return route('categories.index');
    }

    public function setDataCreate($data = [])
    {
        $name = $this->faker->unique->name;
        return array_merge([
            'name' => strtoupper($name),
            'slug' => Str::slug($name)
        ], $data);
    }

    /** @test */
    public function authenticated_super_admin_can_see_create_category_form()
    {
        $this->loginAsSuperAdmin();
        $response = $this->get($this->getCreateRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.categories.create');
        $response->assertSee('Category');
    }

    /** @test */
    public function unauthenticated_user_can_not_see_create_category_form()
    {
        $response = $this->get($this->getCreateRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticates_user_without_permission_can_not_see_create_category_form()
    {
        $this->loginAsUser();
        $response = $this->get($this->getCreateRoute());
        $response->assertStatus(Response::HTTP_OK);
    }
    /** @test */
    public function authenticates_super_admin_can_create_new_category()
    {
        $this->loginAsSuperAdmin();
        $data = $this->setDataCreate();
        $categoryCountBefore = Category::count();
        $response = $this->post($this->getStoreRoute(), $data);
        $categoryCountAfter = Category::count();
        $this->assertEquals($categoryCountBefore + 1, $categoryCountAfter);
        $this->assertDatabaseHas('categories', ['name' => $data['name']]);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getCategoryRoute());
    }

    /** @test */
    public function unauthenticated_user_can_not_create_category()
    {
        $data = $this->setDataCreate();
        $response = $this->post($this->getStoreRoute(), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function category_has_many_product()
    {
        $category = Category::factory()->create();
        $product = Product::factory()->create();
        $category->products()->sync($product->pluck('id'));
        $this->assertDatabaseHas('product_category', ['category_id' => $category->id, 'product_id' => $product->id]);
    }

    /** @test */
    public function authenticated_super_admin_can_not_create_new_category_if_name_is_null()
    {
        $this->loginAsSuperAdmin();
        $data = $this->setDataCreate(['name' => null]);
        $response = $this->post($this->getStoreRoute(), $data);
        $response->assertSessionHasErrors('name');
    }

    /** @test */
    public function authenticated_user_has_permission_can_see_create_category_form()
    {
        $this->loginAsUserWithPermission('categories-create');
        $response = $this->get($this->getCreateRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.categories.create');
        $response->assertSee('Categories');
    }

    /** @test */
    public function authenticated_user_has_permission_can_create_new_category()
    {
        $this->loginAsUserWithPermission('categories-store');
        $data = $this->setDataCreate();
        $categoryCountBefore = Category::count();
        $response = $this->post($this->getStoreRoute(), $data);
        $categoryCountAfter = Category::count();
        $this->assertEquals($categoryCountBefore + 1, $categoryCountAfter);
        $this->assertDatabaseHas('categories', ['name' => $data['name']]);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getCategoryRoute());
    }

    /** @test */
    public function authenticated_user_has_permission_can_not_create_new_category_if_name_is_null()
    {
        $this->loginAsUserWithPermission('categories-store');
        $data = $this->setDataCreate(['name' => null]);
        $response = $this->post($this->getStoreRoute(), $data);
        $response->assertSessionHasErrors('name');
    }

}
