<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListCategoryTest extends TestCase
{
    use WithFaker;

    public function getRoute()
    {
        return route('categories.index');
    }

    /** @test  */
    public function super_admin_can_get_all_categories()
    {
        $this->loginAsSuperAdmin();
        $category = Category::factory()->create();
        $response = $this->get($this->getRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.categories.index');
        $response->assertSee($category->name);
    }

    /** @test  */
    public function authenticated_user_without_permission_can_not_get_all_categories()
    {
        $this->loginAsUser();
        $response = $this->get($this->getRoute());
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test  */
    public function authenticated_user_has_permission_can_get_all_categories()
    {
        $this->loginAsUserWithPermission('categories-index');
        $category = Category::factory()->create();
        $response = $this->get($this->getRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.categories.index');
        $response->assertSee($category->name);
    }

    /** @test  */
    public function unauthenticated_user_can_not_get_all_categories()
    {
        $category = Category::factory()->create();
        $response = $this->get($this->getRoute());
        $response->assertRedirect('/login');
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertDontSee($category->name);
    }

}
