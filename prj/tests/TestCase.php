<?php

namespace Tests;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\WithFaker;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use WithFaker;

    protected function loginAsSuperAdmin()
    {
        $user = User::factory()->create();
        $role = Role::where('name', 'super_admin')->pluck('id');
        $user->roles()->attach($role);

        return $this->actingAs($user);
    }

    protected function loginAsUser()
    {
        $user = User::factory()->create();
        return $this->actingAs($user);
    }

    protected function loginAsUserWithPermission($permission)
    {
        $user = User::factory()->create();
        $role = Role::factory()->create();
        $user->roles()->attach($role->id);
        $permission = Permission::where('name', $permission)->first();
       // $role->permissions()->attach($permission->id);
        return $this->actingAs($user);
    }

}
