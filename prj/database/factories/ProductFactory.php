<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => $this->faker->unique->name,
            'price' => $this->faker->numberBetween($min = 1, $max = 100000),
            'image' => '1656496044-defaul_image.jpg',
            'describe' => $this->faker->text,
        ];
    }
}
