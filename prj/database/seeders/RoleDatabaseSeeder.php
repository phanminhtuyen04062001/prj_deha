<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RoleDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            ['name' => 'super-admin', 'display_name' =>'Super Admin'],
            ['name' => 'admin', 'display_name' =>'Admin'],
            ['name' => 'employee', 'display_name' =>'employee'],
            ['name' => 'manager', 'display_name' =>'manager'],
            ['name' => 'user', 'display_name' =>'user'],
        ];

        foreach ($roles as $role){
            Role::updateOrCreate($role);
        }

        $permissions = [
           ['name' => 'create-user', 'display_name' =>'Create user', ],
            ['name' => 'update-user', 'display_name' =>'Update user'],
            ['name' => 'show-user', 'display_name' =>'Show user'],
            ['name' => 'delete-user', 'display_name' =>'Delete user'] ,

            ['name' => 'create-role', 'display_name' =>'Create role'],
            ['name' => 'update-role', 'display_name' =>'Update role'],
            ['name' => 'show-role', 'display_name' =>'Show role'],
            ['name' => 'delete-role', 'display_name' =>'Delete role'],

            ['name' => 'create-categories', 'display_name' =>'Create categories'],
            ['name' => 'update-categories', 'display_name' =>'Update categories'],
            ['name' => 'show-categories', 'display_name' =>'Show categories'],
            ['name' => 'delete-categories', 'display_name' =>'Delete categories'],

            ['name' => 'create-product', 'display_name' =>'Create product'],
            ['name' => 'update-product', 'display_name' =>'Update product'],
            ['name' => 'show-product', 'display_name' =>'Show product'],
            ['name' => 'delete-product', 'display_name' =>'Delete product'],

            ['name' => 'create-coupon', 'display_name' =>'Create coupon'],
            ['name' => 'update-coupon', 'display_name' =>'Update coupon'],
            ['name' => 'show-coupon', 'display_name' =>'Show coupon'],
            ['name' => 'delete-coupon', 'display_name' =>'Delete coupon'],

            ];
        foreach ($permissions as $item) {
            Permission::updateOrCreate($item);
        }
    }
}
