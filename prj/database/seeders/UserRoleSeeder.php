<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;

class UserRoleSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_role')->insert([
            [
                'role_id' => 3,
                'user_id' => 1
            ]
        ]);
    }
}
