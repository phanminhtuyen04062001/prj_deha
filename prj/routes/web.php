<?php

use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/







Auth::routes();
//
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])
    ->name('home');

Route::middleware(['auth'])->group(function() {


    Route::name('roles.')->prefix('roles')->group(function () {

        Route::get('/', [RoleController::class, 'index'])
            ->name('index')
        ->middleware('permission:view-roles');

        Route::get('/create', [RoleController::class, 'create'])
            ->name('create')
        ->middleware('permission:create-roles');

        Route::get('/show/{id}', [RoleController::class, 'show'])
            ->name('show')
        ->middleware('permission:show-roles');

        Route::get('/edit/{id}', [RoleController::class, 'edit'])
            ->name('edit')
        ->middleware('permission:edit-roles');

        Route::put('/update/{id}', [RoleController::class, 'update'])
            ->name('update')
        ->middleware('permission:update-roles');

        Route::delete('/destroy/{id}', [RoleController::class, 'destroy'])
            ->name('destroy')
        ->middleware('permission:destroy-roles');

        Route::post('/', [RoleController::class, 'store'])
            ->name('store')
        ->middleware('permission:store-roles');

    });

    Route::name('users.')->prefix('users')->group(function () {

        Route::get('/', [UserController::class, 'index'])
            ->name('index')
        ->middleware('permission:view-user');

        Route::get('/create', [UserController::class, 'create'])
            ->name('create')
        ->middleware('permission:create-user');

        Route::post('/store', [UserController::class, 'store'])
            ->name('store')
        ->middleware('permission:store-user');

        Route::get('/show/{id}', [UserController::class, 'show'])
            ->name('show')
        ->middleware('permission:show-user');

        Route::get('/edit/{id}', [UserController::class, 'edit'])
            ->name('edit')
        ->middleware('permission:edit-user');

        Route::put('/update/{id}', [UserController::class, 'update'])
            ->name('update')
        ->middleware('permission:update-user');

        Route::delete('/destroy/{id}', [UserController::class, 'destroy'])
            ->name('destroy')
        ->middleware('permission:destroy-user');

    });
    Route::name('categories.')->prefix('categories')->group(function () {

        Route::get('/', [CategoryController::class,'index'])
            ->name('index')
            ->middleware('permission:index-category');

        Route::get('/create', [CategoryController::class,'create'])
            ->name('create')
            ->middleware('permission:create-category');

        Route::post('/store', [CategoryController::class,'store'])
            ->name('store')
            ->middleware('permission:store-category');

        Route::get('/show/{id}', [CategoryController::class,'show'])
            ->name('show')
            ->middleware('permission:show-category');

        Route::get('/edit/{id}', [CategoryController::class,'edit'])
            ->name('edit')
            ->middleware('permission:edit-category');

        Route::put('/update/{id}', [CategoryController::class,'update'])
            ->name('update')
            ->middleware('permission:update-category');

        Route::delete('/destroy/{id}', [CategoryController::class,'destroy'])
            ->name('destroy')
            ->middleware('permission:destroy-category');

    });


    Route::name('products.')->prefix('products')->group(function () {

        Route::get('/', [ProductController::class, 'index'])
            ->name('index')
            ->middleware('permission:view-product');
        Route::get('/table', [ProductController::class, 'table'])
            ->name('table')
            ->middleware('permission:view-product');

        Route::get('/search', [ProductController::class, 'search'])
            ->name('search')
            ->middleware('permission:view-product');

        Route::get('/edit/{id}', [ProductController::class, 'edit'])
            ->name('edit')
            ->middleware('permission:edit-product');

        Route::post('/store', [ProductController::class, 'store'])
            ->name('store')
            ->middleware('permission:store-product');

        Route::get('/show/{id}', [ProductController::class, 'show'])
            ->name('show')
            ->middleware('permission:show-product');

        Route::get('/create', [ProductController::class, 'create'])
            ->name('create')
            ->middleware('permission:create-product');

        Route::put('/update/{id}', [ProductController::class, 'update'])
            ->name('update')
            ->middleware('permission:update-product');

        Route::delete('/delete.{id}', [ProductController::class, 'delete'])
            ->name('delete')
            ->middleware('permission:delete-product');
    });
});

