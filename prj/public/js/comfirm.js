
const comfirmAlert = {
    confirm: function (){
        return swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this imaginary file!",
            icon: "warning",
            buttons: true,
            dangerMode: true
        });
    },
    showSuccessMessageAlert: function showSuccessMessageAlert(title)
    {
        toastr.success(title);
    },

    showErrorMessage: function showErrorMessage(errors)
    {
        for(let key in errors)
        {
            $(".error-"+key).html(errors[key]);
        }
    },
    resetError: function resetError()
    {
        $('.error-reset').html("");
    }
};

export default comfirmAlert;
