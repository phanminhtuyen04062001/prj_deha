const Products = (function () {
    let modules = {};
    modules.getList = function () {
        let url = $('#show-product').attr('data-action-table');
        var formData = $('#search-product').serialize();
        Base.callApiData(url, formData,'GET' ).then(function (res) {
            $('#show-product').html('').append(res);
        });

    }

    modules.getListByPage = function (e) {
        let url = e.attr('href');
        Base.callNormalApi(url).then(function (res) {
            $('#show-product').html('').append(res);
        })
    }

    modules.getShowData = function (element) {
        let url = element.attr('data-url-show');
        Base.callNormalApi(url).then(function (res) {
            $('#product-name').text(res.resource.name);
            $('#product-image').attr('src', res.resource.image);
            $('#product-price').text(res.resource.price);
            $('#product-description').text(res.resource.describe);
            var categories = "";
            res.resource.categories.forEach(category => {
                categories = category.name + "/ ";
            });
            $('#product-category').text(categories);
            Products.checkImage(res.resource.image, 'product-imaged');
        })
    }

    modules.show = function () {
        $('#modal-show').modal('show');
        $('#close-show').click(function () {
            $('#modal-show').modal('hide');
        })
    }
    modules.add = function () {
        $('#modal-add').modal('show');
        $('#close-add').click(function () {
            $('#modal-add').modal('hide');
            Products.resetAddForm();
        })
    }

    modules.store = function () {
        let url = $(".btn-store").attr("data-url");
        var formData = new FormData(document.getElementById("form-add"));
        Base.callApiData(url, formData)
            .then(function (res) {
                $('#modal-add').modal('hide');
                Products.resetAddForm();
                Products.getList($('#list').data('data-url'));
                CustomAlert.messageSuccessModal(res.message);
            })
            .fail(function (res){
                console.log(res)
                CustomAlert.showModalError(JSON.parse(res.responseText).errors);
            });
    }

    modules.deleteProduct = function (element) {
        let url = element.attr('data-url');
        Base.callNormalApi(url, 'DELETE').then(function (res) {
            Products.getList();
        })
    }

    modules.getEditData = function (element) {
        let url = element.attr('data-url');
        Base.callNormalApi(url).then(function (res) {
            $('#image-edit').attr('src', res.resource.image);
            $('#name-edit').val(res.resource.name);
            $('#price-edit').val(res.resource.price);
            $('#description-edit').val(res.resource.describe);
            $('#btn-update').attr('data-action', res.resource.route);
            res.resource.categories.forEach(category => {
                $(`#category-edit option[value="${category.category_id}"]`).attr('selected', 'selected');
            });
            Products.checkImage(res.resource.image, 'product-image-edit');
        })
    }

    modules.edit = function () {
        $('#modal-edit').modal('show');
        $('#close-edit').click(function () {
            $('#modal-edit').modal('hide');
            Products.resetEditForm();
        })
    }

    modules.update = function (element) {
        let url = $("#btn-update").data("action");
        var formData = new FormData(document.getElementById("form-edit"));
        formData.append('_method', 'put');
        CustomAlert.resetError();
        Base.callApiData(url, formData)
            .done(function (res) {
                Products.resetEditForm();
                $('#modal-edit').modal('hide');
                CustomAlert.messageSuccessModal(res.message);
                Products.getList();
            }).fail(function (res){
                console.log(res);
                CustomAlert.showModalError(JSON.parse(res.responseText).errors);
            })
    }

    modules.previewImage = function (e, img) {
        var output = document.getElementById(img);
        output.src = URL.createObjectURL(e.target.files[0]);
        output.setAttribute("style", "max-width: 30%; height: auto;");
        output.onload = function () {
            URL.revokeObjectURL(output.src)
        }
    }

    modules.checkImage = function (imgSrc, id) {
        var img = new Image();
        img.onload = function () {
            $('#' + id).attr('src', imgSrc);
        };
        img.onerror = function () {
            $('#' + id).attr('src', 'upload/default_image.jpg');
        };
        img.src = imgSrc;
    }

    modules.resetAddForm = function () {
        $("#form-add").trigger("reset");
        $(".create-error").empty();
    }

    modules.resetEditForm = function () {
        $("#form-edit").trigger("reset");
        $(".edit-error").empty();
        $("#product-image-edit").attr('src', '');
        $("#category-edit option:selected").attr("selected", false);
    }

    modules.choseImage = function (id){
        $("#image-edit").trigger("click");
    }
    return modules;
})();

$(document).ready(function () {
    Products.getList();

    $(document).on('click', '.btn-show', function (e) {
        e.preventDefault();
        Products.getShowData($(this));
        Products.show();
    });

    $(document).on('click', '.btn-add', function (e) {
        e.preventDefault();
        Products.add();
    });

    $(document).on('submit', '#form-add', function (e) {
        e.preventDefault();
        Products.store($(this));
    });

    $(document).on('click', '.btn-delete', function (e) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                Products.deleteProduct($(this));
                Swal.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                )
            }
        })
        e.preventDefault();

    });

    $(document).on('click', '.btn-edit', function (e) {
        e.preventDefault();
        Products.getEditData($(this))
        Products.edit();
    });

    $(document).on('submit', '#form-edit', function (e) {
        e.preventDefault();
        Products.update($(this));
    });

    $(document).on('click', '#product-image-edit', function (e){
        e.preventDefault();
        Products.choseImage('image-edit');
    });

    $(document).on('click', '.pagination a', function (e) {
        e.preventDefault();
        Products.getListByPage($(this));
    })

    $(document).on('click', '#search-product', function (e) {
        e.preventDefault();
        Products.getList();
    })

    $(document).on('change', '#image-add', function (e) {
        e.preventDefault();
        Products.previewImage(e, 'img-add-output');
    })

    $(document).on('change', '#image-edit', function (e) {
        e.preventDefault();
        Products.previewImage(e, 'product-image-edit');
    })
});
