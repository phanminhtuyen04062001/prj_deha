$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
const Base = (function () {
    let modules = {};

    modules.callNormalApi = function (url, method='get', data){
        return $.ajax({
             url,
            method: method,
            data,
        })
    }

    modules.callApiData = function (url, data = {}, method='POST'){
        return $.ajax({
             url,
            method: method,
             data,
            contentType: false,
            processData: false,
        })
    }

    return modules;

})();
const CustomAlert = (function (){
    let modules = {}

    modules.messageSuccessModal = function (message) {
        Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: message,
            showConfirmButton: true,
        })
    }

    modules.showError = function (obj){
        $.each(obj, function (key,value) {
            $(`.error-${key}`).html(value);
        });
    };

    modules.resetError = function () {
        $('.error').html('');
    };

    modules.showModalError = function (error) {
        Swal.fire({
            icon: 'error',
            title: 'Warning...',
            text: 'Something went wrong!',
        })
    };
    return modules;
}(window.jQuery, window, document));

