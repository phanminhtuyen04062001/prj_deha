@extends('admin.Layouts.app')
@section('title','Categories')

@section('content')
    <div class="card">

        @if (session('massage'))
            <h1 class="text-primary">{{ session('massage') }}</h1>
        @endif

        <h1>
            Category list
        </h1>
        <div>
            <a href="{{ route('categories.create') }}" class="btn btn-primary">Create</a>
        </div>
    </div>

    <table class="table table-hover">
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Parent</th>
            <th>Action</th>
        </tr>
        @foreach ($categories as $category)
            <tr>
                <td>{{ $category->id }}</td>
                <td>{{ $category->name }}</td>
                @if($category->parent_id)
                <td>{{ $category->parent->name }}</td>

                @else
                <td>Parent Category</td>

                @endif
                <td>
                    <a href="{{ route('categories.edit', $category->id) }}" class="btn btn-success">Edit</a>
                    <a href="{{ route('categories.show', $category->id) }}" class="btn btn-warning">Show</a>
                    <form action="{{ route('categories.destroy', $category->id) }}" method="post" style="display: inline">
                        @csrf
                        @method('delete')
                        <button  class="btn btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>
    {{ $categories->links('vendor.pagination.bootstrap-4') }}
@endsection
