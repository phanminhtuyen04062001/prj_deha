@extends('admin.Layouts.app')
@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Category Management</h4>
                        <form action="" >
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Name</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="name" placeholder="Name ..." value="{{$category->name}}"/>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Danh mục</label>
                                        @foreach($category->children as $iterm)
                                            <span class="label success">{{$iterm->name}}</span>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
{{--                            <div class="row">--}}
{{--                                <div class="col-md-12">--}}
{{--                                    <div class="form-group row">--}}
{{--                                        <label class="col-sm-2 col-form-label">Products</label>--}}
{{--                                        @foreach($category->products as $iterm)--}}
{{--                                            <span class="label success">{{$iterm->name}}</span>--}}
{{--                                        @endforeach--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                        </form><br>
                        <div class="row">
                            <div class="col-md-12">
                                <a href="{{ route('categories.index') }}"  class="btn btn-primary">Quay lại</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
