<div class="container modal fade" id="modal-edit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div style="margin: 20px 20px 20px 20px;">
                <h1 id="title-edit">Edit Product </h1>
                <div class="mb-3">
                    <a style="float:right" class="btn btn-secondary" id="close-edit">Back</a>
                    </br>
                </div>
                <div id="product-id"></div>
                <form id="form-edit" enctype="multipart/form-data" data-action="" method="put">
                    <div class="mb-3">
                        <label class="form-label">Name </label>
                        <input type="text" name="name" class="form-control" id="name-edit">
                        <div class="text-danger edit-error"><span id="name-error"></span></div>
                        @error('name')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="mb-3">
                        <label class="form-label">Price </label>
                        <input type="number" name="price" class="form-control" id="price-edit">
                        <div class="text-danger edit-error"><span id="price-error"></span></div>
                        @error('price')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="mb-3">
                        <label class="form-label">Describe</label>
                        <input type="text" name="description" class="form-control" id="description-edit">
                        <div class="text-danger edit-error"><span id="description-error"></span></div>
                        @error('description')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="mb-3">
                        <label class="form-label">Image </label></br>
                        <img class="rounded bg-light image-data img-thumbnail" accept="image/*"
                             style="file" id="product-image-edit" src=""/>
                        <input accept="image/*" type="file" name="image" class="form-control" placeholder="Post Image"
                               id="image-edit" hidden>
                        <div class="text-danger edit-error"><span id="image-error"></span></div>
                        @error('image')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="mb-3">
                        <label class="form-label">Category</label>
                        <select class="form-select" name="category_ids[]" id="category-edit" multiple>
                            @foreach ($categories as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" id="btn-update"   class="btn btn-primary btn-update"
                            data-action="">Submit
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
