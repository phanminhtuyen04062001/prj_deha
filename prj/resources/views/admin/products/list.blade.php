<div id="show-product">
    <div class="table-responsive pt-3">
        <div style="display: none;">{{$stt = 0}}</div>
        <table class="table table hover">
            <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Price</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <div id="show_product"></div>
            @foreach ($products as $product)
                <tr>
                    <th>{{ $stt=$stt+1 }}</th>
                    <th>{{$product->name}}</th>
                    <th>{{$product->price}}</th>
                    <td>
                        <a class="btn btn-info btn-show"
                           data-url-show="{{route('products.show',$product->id)}}">Show</a>

                        <a class="btn btn-warning btn-edit" data-url="{{route('products.edit',$product->id)}}">Edit</a>

                        <button type="button" class="btn btn-danger btn-delete" value="{{$product->id}}"
                                data-url="{{route('products.delete',$product->id)}}">Delete
                        </button>
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>
    </div>
</div>
{{ $products->links('vendor.pagination.bootstrap-4') }}

