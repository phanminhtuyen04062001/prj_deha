<div class="container modal fade" id="modal-add">
    <div class="modal-dialog">
        <div class="modal-content">
            <div style="margin: 20px 20px 20px 20px;">
                <h1>Add New Product</h1>
                <div class="mb-3">
                    <a style="float:right" class="btn btn-secondary" id="close-add">Back</a>
                    </br>
                </div>
                <form id="form-add" method="post" enctype="multipart/form-data">
                    <div class="mb-3">
                        <label class="form-label"><strong>Name </strong></label>
                        <input type="text" name="name" class="form-control" id="name-add">
                        <div class="text-danger create-error"><span id="name-error"></span></div>
                    </div>

                    <div class="mb-3">
                        <label class="form-label"><strong>Price</strong> </label>
                        <input type="number" name="price" class="form-control" id="price-add">
                        <div class="text-danger create-error"><span id="price-error"></span></div>
                    </div>

                    <div class="mb-3">
                        <label class="form-label"><strong>Describe</strong></label>
                        <input type="text" name="describe" class="form-control" id="describe-add">
                        <div class="text-danger create-error"><span id="description-error"></span></div>
                    </div>

                    <div class="mb-3">
                        <label class="form-label"><strong>Image</strong> </label>
                        <img id="img-add-output"/>
                        <input type="file" name="image" accept="image/*" class="form-control" id="image-add">
                        <div class="text-danger create-error"><span id="image-error"></span></div>
                    </div>

                    <div class="mb-3">
                        <label class="form-label"><strong>Category</strong></label>
                        <select class="form-select" name="category_ids[]" id="category-add" multiple>
                            @foreach ($categories as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <button class="btn btn-primary btn-store"  id="btn-create"
                            data-url="{{route('products.store')}}">Submit
                    </button>
                </form>

            </div>


        </div>
    </div>
</div>
