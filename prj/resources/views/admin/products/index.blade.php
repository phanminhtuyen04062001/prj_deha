@extends('admin.Layouts.app')

@section('content')
    <div class="container">

        @if (session('message'))
            <div class="alert alert-primary" role="alert">
                {{ session('message') }}
            </div>
        @endif
        <div>
            <h1>LIST PRODUCT</h1>
        </div>
        <div>

            <a class="btn btn-primary btn-add" data-url="{{route('products.create')}}">Add</a>

        </div>
        <br>
        <div class="form-search">
            <form method="get" id="search-product">
                <div class="mb-3" style="display:flex">
                    <label class="form-label">Name&emsp;</label>
                    <input value="{{request()->name ?? ''}}" placeholder="abcd" type="text" name="name"
                           class="form-control"
                           style="padding-left: 10px; background-color: #fff; border: 1px solid #e7ebf0;">
                </div>
                <div class="mb-3" style="display:flex">
                    <label class="form-label">Price&emsp;</label>
                    <input value="{{request()->min_price ?? ''}}" placeholder="Min Price" type="number" name="min_price"
                           step="10000" min="0" class="form-control"
                           style="padding-left: 10px; background-color: #fff; border: 1px solid #e7ebf0;">
                    <input value="{{request()->max_price ?? ''}}" placeholder="Max Price" type="number" name="max_price"
                           step="10000" min="0" class="form-control"
                           style="padding-left: 10px; background-color: #fff; border: 1px solid #e7ebf0;">
                </div>
                <div class="mb-3" style="display:flex">
                    <label class="form-label">Category&emsp;</label>
                    <select class="form-select" name="category" type="text"
                            style="padding-left: 10px; background-color: #fff; border: 1px solid #e7ebf0;">
                        <option value="" >Select</option>
                        @foreach($categories as $category)
                            <option
                                value="{{$category->id}}" {{ request()->category_id == $category->id ? 'selected' : '' }}>{{$category->name}}</option>
                        @endforeach
                    </select>
                </div>
                <button class="btn btn-success search-product" type="submit">Search</button>
            </form>
        </div>
        <div>
            <div class="row" id="show-product" data-action-table="{{route('products.table')}}"></div>
        </div>


    </div>
    @include('admin.products.show')
    @include('admin.products.create')
    @include('admin.products.edit')
@endsection
<script src="{{asset('js/jquery.min.js')}}" defer></script>
<script src="{{asset('js/base.js')}}" defer></script>
<script src="{{asset('js/product.js')}}" defer></script>
<script src="{{asset('js/toastr.js')}}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

