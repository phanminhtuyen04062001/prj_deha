@extends('admin.Layouts.app')
@section('title','Edit Users'.$user->name)
@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">User Management</h4>
                        <form action="{{ route('users.update', $user->id) }}" method="POST" class="form-sample">
                            @csrf
                            @method('PUT')
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Name</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="name" value="{{ $user->name }}"/>
                                        </div>
                                    </div>
                                    @error('name')
                                    <div class="errors">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">E-Mail</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="email" value="{{ $user->email }}"/>
                                        </div>
                                    </div>
                                    @error('email')
                                    <div class="errors">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Phone</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="email" value="{{ $user->phone }}"/>
                                        </div>
                                    </div>
                                    @error('phone')
                                    <div class="errors">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Password</label>
                                        <div class="col-sm-9">
                                            <input type="password" class="form-control" name="password"/>
                                        </div>
                                    </div>
                                    @error('password')
                                    <div class="errors">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-1 col-form-label">Roles</label>
                                        <div class="col-sm-9" style="padding-left: 50px;">
                                            @foreach($roles as $iterm)
                                                <label>
                                                    <input type="checkbox" value="{{ $iterm->id }}"  {{$user->roles->contains('id', $iterm->id) ? 'checked' : ''}} name="role_ids[]" class="option-input checkbox"  />
                                                    {{ $iterm->display_name }}
                                                </label>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @error('role_ids')
                            <div  class="errors">{{ $message }}</div>
                            @enderror
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary mr-2">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
