@extends('admin.Layouts.app')
@section('title','Create Users')
@section('content')
    <div class="card">
        <h1>Create user</h1>
    </div>
    <div>
    <form action="{{ route('users.store') }}" method="post" enctype="multipart/form-data">
        @csrf


        <div class="input-group input-group-static mb-4">
            <label>Name</label>
            <input type="text" value="{{ old('name') }}" name="name" class="form-control">

             @error('name')
            <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>

        <div class="input-group input-group-static mb-4">
            <label> Email</label>
            <input type="email" value="{{old('email')}}" name="email" class="form-control">

            @error('email')
            <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>


        <div class="input-group input-group-static mb-4">
            <label> Phone </label>
            <input type="text" value="{{old('phone')}}" name="phone" class="form-control">

            @error('phone')
            <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>


        <div class="input-group input-group-static mb-4">
            <label  name="group" class="ms-0">Group</label>
            <select name="gender" class="form-control" >
                <option value="male">Male</option>
                <option value="fe-male">FeMale</option>

            </select>
            @error('gender')
            <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>


        <div class="input-group input-group-static mb-4">
            <label> Address </label>
            <textarea  name="address" class="form-control">{{ old('address') }}</textarea>

            @error('address')
            <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>

        <div class="input-group input-group-static mb-4">
            <label> Password </label>
            <input type="password"  name="password" class="form-control">

            @error('password')
            <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>
        <div class="mb-3">
            <label class="form-label">Role</label>
            </br>
            <select class="form-select" name="role_ids[]" multiple size="8">
                @foreach($roles as $role)
                    <option value="{{$role->id}}">{{ $role->display_name }}</option>
                @endforeach
            </select>

        </div>
        <button type="submit" class="btn btn-submit btn-primary">Submit</button>
    </form>
    </div>
    </div>
@endsection
