@extends('admin.Layouts.app')
@section('title','Users')

@section('content')
    <div class="card">

        @if (session('massage'))
            <h1 class="text-primary">{{ session('massage') }}</h1>
        @endif

    <h1>
        User list
    </h1>
        <div>
            <a href="{{ route('users.create') }}" class="btn btn-primary">Create</a>
        </div>
    </div></br>
    <div>
        <form action="{{route('users.index')}}" method="get">
            <div class="mb-3" style="display:flex">
                <label class="form-label">Name&emsp;</label>
                <input value="{{request()->name}}" placeholder="abcd" type="text" name="name" class="form-control" style="padding-left: 10px; background-color: #fff; border: 1px solid #e7ebf0;">
            </div>
            <div class="mb-3" style="display:flex">
                <label class="form-label">Email&emsp;</label>
                <input value="{{request()->email}}" placeholder="abcd" type="text" name="email"  class="form-control" style="padding-left: 10px; background-color: #fff; border: 1px solid #e7ebf0;">
            </div>
            <div class="mb-3" style="display:flex">
                <label class="form-label">Role&emsp;</label>
                <select class="form-select" name="role_id" style="padding-left: 10px; background-color: #fff; border: 1px solid #e7ebf0;">
                    <option value="">Select</option>
                    @foreach($roles as $role)
                        <option value="{{$role->id}}" {{ request()->role_id == $role->id ? 'selected' : '' }}>{{$role->display_name}}</option>
                    @endforeach
                </select>
            </div>
            <button class="btn btn-success" type="submit">Search</button>
        </form>
    </div>

    <table class="table table-hover" >
        <tr >
            <th>#</th>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Action</th>
        </tr>
        @foreach ($users as $item)
            <tr>
                <td>{{ $item->id }}</td>
                <td>{{ $item->name }}</td>
                <td>{{ $item->email }}</td>
                <td>{{ $item->phone }}</td>
                <td>
                    <a href="{{ route('users.edit', $item->id) }}" class="btn btn-success">Edit</a>
                    <a href="{{ route('users.show', $item->id) }}" class="btn btn-warning">Show</a>
                    <form action="{{ route('users.destroy', $item->id) }}" method="post" style="display: inline">
                        @csrf
                        @method('delete')
                    <button  class="btn btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>
    {{ $users->links('vendor.pagination.bootstrap-4') }}
@endsection
