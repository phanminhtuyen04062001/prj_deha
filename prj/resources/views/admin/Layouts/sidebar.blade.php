<aside
    class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3   bg-gradient-dark"
    id="sidenav-main">
    <div class="sidenav-header">
        <i class="fas fa-times p-3 cursor-pointer text-white opacity-5 position-absolute end-0 top-0 d-none d-xl-none"
           aria-hidden="true" id="iconSidenav"></i>
{{--       <img src="https://demos.creative-tim.com/material-dashboard/assets/img/logo-ct.png">--}}
        <a class="navbar-brand m-0" href=" https://demos.creative-tim.com/material-dashboard/pages/dashboard " target="_blank">
            <img src="https://demos.creative-tim.com/material-dashboard/assets/img/logo-ct.png" class="navbar-brand-img h-100" alt="main_logo">
            <span class="ms-1 font-weight-bold text-white">Admin</span>
        </a>
    </div>
    <hr class="horizontal light mt-0 mb-2">
    <div class="collapse navbar-collapse  w-auto  max-height-vh-100" id="sidenav-collapse-main">
        <ul class="navbar-nav "  >
{{--            <li class="nav-item">--}}
{{--                <a class="nav-link text-white  {{ request()->route('roles.*') ? 'bg-gradient-primary active' : '' }}"--}}
{{--                   href="{{ route('roles.index') }}">--}}
{{--                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">--}}
{{--                        <i class="material-icons opacity-10">dashboard</i>--}}
{{--                    </div>--}}
{{--                    <span class="nav-link-text ms-1">Roles</span>--}}
{{--                </a>--}}

{{--            </li>--}}
            <li class="nav-item">
                <a class="nav-link text-white {{ request()->routeIs('roles.*') ? 'btn btn-success active' : '' }}" href="{{route('roles.index')}}">
                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="material-icons opacity-10">table_view</i>
                    </div>
                    <span class="nav-link-text ms-1">Role</span>
                </a>
            </li>
{{--            <li class="nav-item">--}}
{{--                <a class="nav-link text-white {{ request()->route('users.*') ? 'bg-gradient-primary active' : '' }} "--}}
{{--                   href="{{ route('users.index') }}">--}}
{{--                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">--}}
{{--                        <i class="material-icons opacity-10">table_view</i>--}}
{{--                    </div>--}}
{{--                    <span class="nav-lrink-text ms-1">User</span>--}}
{{--                </a>--}}
{{--            </li>--}}
            <li class="nav-item">
                <a class="nav-link text-white {{ request()->routeIs('users.*') ? 'btn btn-secondary active' : '' }}" href="{{route('users.index')}}">
                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="material-icons opacity-10">table_view</i>
                    </div>
                    <span class="nav-link-text ms-1">Users</span>
                </a>
            </li>
{{--            <li class="nav-item">--}}
{{--                <a class="nav-link text-white {{ request()->route('categories.*') ? "bg-gradient-primary " : '' }} "--}}
{{--                   href="{{ route('categories.index') }}">--}}
{{--                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">--}}
{{--                        <i class="material-icons opacity-10">receipt_long</i>--}}
{{--                    </div>--}}
{{--                    <span class="nav-link-text ms-1">Category</span>--}}
{{--                </a>--}}
{{--            </li>--}}
            <li class="nav-item">
                <a class="nav-link text-white {{ request()->routeIs('categories.*') ? 'btn btn-success active' : '' }}" href="{{route('categories.index')}}">
                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="material-icons opacity-10">table_view</i>
                    </div>
                    <span class="nav-link-text ms-1">Category</span>
                </a>
            </li>
{{--            <li class="nav-item">--}}
{{--                <a class="nav-link text-white {{ request()->route('products.*') ? "btn btn-danger " : '' }} "--}}
{{--                   href="{{ route('products.index') }}">--}}
{{--                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">--}}
{{--                        <i class="material-icons opacity-10">view_in_ar</i>--}}
{{--                    </div>--}}
{{--                    <span class="nav-link-text ms-1">Product</span>--}}
{{--                </a>--}}
{{--            </li>--}}
            <li class="nav-item">
                <a class="nav-link text-white {{ request()->routeIs('products.*') ? 'btn btn-success active' : '' }}" href="{{route('products.index')}}">
                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="material-icons opacity-10">table_view</i>
                    </div>
                    <span class="nav-link-text ms-1">Product</span>
                </a>
            </li>

        </ul>
    </div>
    <div class="sidenav-footer position-absolute w-100 bottom-0 ">
        <div class="mx-3">
{{--            <a class="btn bg-gradient-primary mt-4 w-100"--}}
{{--               href="https://www.creative-tim.com/product/material-dashboard-pro?ref=sidebarfree" type="button">Upgrade--}}
{{--                to pro</a>--}}
        </div>
    </div>
</aside>

