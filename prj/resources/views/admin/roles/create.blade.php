@extends('admin.Layouts.app')
@section('title','Create Roles')

@section('content')
    <div class="card">
        <h1>Create role</h1>
    </div>
    <div>
        <form action="{{ route('roles.store') }}" method="post">
            @csrf
            <div class="input-group input-group-static mb-4">
                <label>Name</label>
                <input type="text" value="{{ old('name') }}" name="name" class="form-control">

                @error('name')
                <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>

            <div class="input-group input-group-static mb-4">
                <label>Display Name</label>
                <input type="text" value="{{old('display_name')}}" name="display_name" class="form-control">

                @error('display_name')
                <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Permission</label>
                <div class="col-sm-9">
                    <select class="js-example-basic-multiple w-100" multiple="multiple" name="permission_ids[]">
                        @foreach($permissions as $iterm)
                            <option value="{{ $iterm->id }}">{{ $iterm->name }}</option>
                        @endforeach
                    </select>
                </div>
                @error('permission_ids')
                <div class="errors">{{ $message }}</div>
                @enderror

            </div>
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary">Create</button>
                </div>
            </div>
        </form>
@endsection

