@extends('admin.Layouts.app')
@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Role Management</h4>
                        <form action="{{route('roles.update',$role->id)}}" method="POST" class="form-sample">
                            @csrf
                            @method('PUT')
                            <p class="card-description">
                                Personal info
                            </p>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Name</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="name"
                                                   value="{{ $role->name }}"/>
                                        </div>
                                    </div>
                                </div>
                                @error('name')
                                <div class="errors">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Name display</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="display_name"
                                                   value="{{ $role->display_name }}"/>
                                        </div>
                                    </div>
                                </div>
                                @error('display_name')
                                <div class="errors">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Permission</label>
                                        <div class="col-sm-9">
                                            <select class="js-example-basic-multiple w-100" multiple="multiple"
                                                    name="permission_ids[]">
                                                @foreach($permissions as $iterm)
                                                    <option
                                                        {{ $role->permissions->contains('id', $iterm->id) ? 'selected' : ''}}  value="{{ $iterm->id }}">{{ $iterm->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="{{ route('roles.index') }}" type="submit" class="btn btn-primary ">Update</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
