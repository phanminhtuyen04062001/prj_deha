<?php

namespace App\Services;

use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Role;

class UserService
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function all()
    {
        return $this->userRepository->all();
    }

    public function store($request)
    {

        $user = $this->userRepository->create($request->all());
        $user->attachRole($request['role_ids']);
        return $user;
    }

    public function update($request, $id)
    {
        $user = $this->userRepository->findOrFail($id);
        return $user->update($request->all());
    }

    public function delete($id)
    {
        $user = $this->userRepository->findOrFail($id);
        $user->delete();
        $user->detachRole();
        return $user;
    }

    public function search($request)
    {
        return $this->userRepository->search($request->all());
    }

    public function findOrFail($id)
    {
        return $this->userRepository->findOrFail($id);
    }
}
