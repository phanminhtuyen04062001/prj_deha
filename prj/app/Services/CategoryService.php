<?php

namespace App\Services;

use App\Http\Requests\CategoryRequest;
use App\Repositories\CategoryRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class CategoryService
{

    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function all()
    {
        return $this->categoryRepository->paginate(5);
    }

    public function store($request)
    {
        $dataCreate = $this->categoryRepository->create($request->all());
        return $dataCreate;
    }

    public function findOrFail($id)
    {
        return $this->categoryRepository->findOrFail($id, 'categories');
    }

    public function update($request, $id)
    {

        $category = $this->categoryRepository->findOrFail($id);
        return $category->update($request->all());
    }

    public function delete($id)
    {
        $category = $this->categoryRepository->delete($id);
        return $category;
    }
}
