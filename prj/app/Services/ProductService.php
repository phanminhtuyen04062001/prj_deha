<?php

namespace App\Services;

use App\Repositories\ProductRepository;
use App\Traits\HandleImage;

class ProductService
{
    use HandleImage;

    private $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function search($request)
    {
        $dataSearch = $request->all();
        $dataSearch['name'] = $request->name;
        $dataSearch['category_id'] = $request->category ?? null;
        $dataSearch['min_price'] = $request->min_price ?? null;
        $dataSearch['max_price'] = $request->max_price ?? null;
        return $this->productRepository->search($dataSearch);
    }

    public function all()
    {
        return $this->productRepository->paginate(5);
    }

    public function findOrFail($id)
    {
        return $this->productRepository->findOrFail($id, 'categories');
    }

    public function create($request)
    {

        $dataCreate = $request->all();
        $dataCreate['category_ids'] = $dataCreate['category_ids'] ?? [];

        $dataCreate['image'] = $this->saveImage($request);
        $product = $this->productRepository->create($dataCreate);
        $product->syncCategories($dataCreate['category_ids']);
        return $product;
    }

    public function update($request, $id)
    {
        $dataUpdate = $request->all();
        $product = $this->productRepository->findOrFail($id);
        $dataUpdate['category_ids'] = $request->category_ids ?? [];
        $dataUpdate['image'] = $this->updateImage($request, $product->image) ;
        $product->update($dataUpdate);
        $product->syncCategories($dataUpdate['category_ids']);
        return $product;
    }

    public function delete($id)
    {
        $product = $this->productRepository->findOrFail($id);
        $product->delete();
        $this->deleteImage($product->image);
        return $product;
    }

    public function count()
    {
        return $this->productRepository->count();
    }
}
