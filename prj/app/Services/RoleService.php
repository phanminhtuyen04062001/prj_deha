<?php

namespace App\Services;

use App\Models\Role;
use App\Repositories\RoleRepository;
use Illuminate\Http\Request;

class RoleService
{
    protected $roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function all()
    {
        return $this->roleRepository->paginate(5);
    }

    public function search($request)
    {
        $dataSearch = $request->input('name');
        return $this->roleRepository->search($dataSearch);
    }

    public function store($request)
    {
        $role = $this->roleRepository->create($request->all());
        return $role;
    }

    public function findOrFail($id)
    {
        $role = $this->roleRepository->findOrFail($id);
        return $role;
    }

    public function update($request, $id)
    {
        $role = $this->roleRepository->findOrFail($id);
        $role->update($request->all());
        $role->syncPermision($request->permission_ids);
        return $role;
    }

    public function delete($id)
    {
        $role = $this->roleRepository->findOrFail($id);
        $role->delete();

        return $role;
    }
}
