<?php

namespace App\Http\Requests\Product;

use App\Rules\IsNumberRule;
use Illuminate\Foundation\Http\FormRequest;

class ProductCreateRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required',
            'price'         => ['required',new IsNumberRule()],
            'describe'      => 'required|max:10',
            'category_ids'  => 'required',
            'image'         => 'required|image|nullable|mimes:jpeg,png,jpg,gif'
        ];
    }
}
