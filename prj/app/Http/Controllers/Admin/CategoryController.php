<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Services\CategoryService;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    protected $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function index(Request $request)
    {
        $categories = $this->categoryService->all();
        return view('admin.categories.index', compact('categories'));
    }

    public function create(Request $request)
    {
        $categories = $this->categoryService->all();
        return view('admin.categories.create', compact('categories'));
    }

    public function store(CategoryRequest $request)
    {
        $this->categoryService->store($request);
        return redirect()->route('categories.index')->with('message', 'Save success');
    }

    public function show($id)
    {
        $category = $this->categoryService->findOrFail($id);
        return view('admin.categories.show', compact('category'));
    }

    public function edit($id)
    {
        $category = $this->categoryService->findOrFail($id);
        $categories = $this->categoryService->all();
        return view('admin.categories.edit', compact('category', 'categories'));
    }

    public function update(CategoryRequest $request, $id)
    {

        $this->categoryService->update($request, $id);
        return redirect()->route('categories.index')->with('message', 'Update success');
    }

    public function destroy($id)
    {
        $this->categoryService->delete($id);
        return redirect()->route('categories.index')->with('message', 'Delete success');
    }
}
