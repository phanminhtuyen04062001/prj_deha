<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Product\ProductCreateRequest;
use App\Http\Requests\Product\ProductUpdateRequest;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use App\Services\CategoryService;
use App\Services\ProductService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProductController extends Controller
{
    protected $categoryservice;


    public function __construct(
        CategoryService $categoryservice,
        ProductService $productService
    ) {
        $this->categoryservice = $categoryservice;
        $this->productService = $productService;
    }

    public function index()
    {
        $products = $this->productService->all();
        $categories = $this->categoryservice->all();
        return view('admin.products.index', compact('products', 'categories'));
    }

    public function create()
    {
        $categories = $this->categoryservice->all();
        $products = $this->productService->all();
        return view('products.create', compact('products', 'categories'));
    }

    public function show($id)
    {
        $product = $this->productService->findOrFail($id);
        return $this->responseSuccsess(new ProductResource($product), 'success', Response::HTTP_OK);
    }

    public function store(ProductCreateRequest $request)
    {

        $product = $this->productService->create($request);
        $productResource = new ProductResource($product);
        return $this->responseSuccsess($productResource, 'Save success', Response::HTTP_OK);
    }

    public function findOrFail($id)
    {
        $product = $this->productService->findOrFail($id);

        return view('products.show', compact('product'));
    }

    public function edit($id)
    {
        $product = $this->productService->findOrFail($id);
        return $this->responseSuccsess(new ProductResource($product), 'successfully', Response::HTTP_OK);
    }

    public function table(Request $request)
    {
        $products = $this->productService->search($request);
        return view('admin.products.list', compact('products'));
    }

    public function update(ProductUpdateRequest $request, $id)
    {
        $product = $this->productService->update($request, $id);
        $productResource = new ProductResource($product);
        return $this->responseSuccsess($product, 'Update success', Response::HTTP_OK);
    }

    public function delete($id)
    {
        $this->productService->delete($id);
        return $this->responseSuccsess('', 'Delete success', Response::HTTP_OK);
    }
}
