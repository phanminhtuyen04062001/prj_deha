<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Services\RoleService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class UserController extends Controller
{
    protected $roleService;
    protected $userService;


    public function __construct(RoleService $roleService, UserService $userService)
    {
        $this->roleService = $roleService;
        $this->userService = $userService;
        View::share('roles', $this->roleService->all());
    }

    public function index(Request $request)
    {
        $users = $this->userService->search($request);
        return view('admin.users.index', compact('users'));
    }

    public function create()
    {
        return view('admin.users.create');
    }

    public function store(UserCreateRequest $request)
    {
        $this->userService->store($request);
        return redirect()->route('users.index')->with('message', 'Save success');
    }

    public function show($id)
    {
        $user = $this->userService->findOrFail($id);
        return view('admin.users.show', compact('user'));
    }

    public function edit($id)
    {
        $user = $this->userService->findOrFail($id);
        return view('admin.users.edit', compact('user'));
    }

    public function update(UserUpdateRequest $request, $id)
    {
        $this->userService->update($request, $id);
        return redirect()->route('users.index')->with('message', 'Update success');
    }

    public function destroy($id)
    {
        $this->userService->delete($id);
        return redirect()->route('users.index')->with('message', 'Delete success');
    }
}
