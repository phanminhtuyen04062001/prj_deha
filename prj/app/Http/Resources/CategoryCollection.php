<?php

namespace App\Http\Resources;

use App\Models\Product;
use Illuminate\Http\Resources\Json\ResourceCollection;

class CategoryCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */


    protected $product;

    public function __construct($resource)
    {
        $this->product = $resource;
        parent::__construct($resource);
    }

    public function toArray($request)
    {
        return $this->product->categories()->get(['category_id', 'name']);
    }
}
