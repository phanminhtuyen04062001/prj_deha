<?php

namespace App\Traits;

use Intervention\Image\Facades\Image;

trait HandleImage
{
    public function verifyImage($request)
    {
        return $request->hasFile('image');
    }

    public function saveImage($request)
    {
        if ($this->verifyImage($request)) {
            $file = $request->file('image');
            $filename = time() . '-' . $file->getClientOriginalName();
            $filepath = config('image.path');
            $image = Image::make($file);
            $image->fit(150, 150)->save($filepath . '/' . $filename);
            return $filename;
        }
        return $this->config('image.imageDefault');
    }

    public function deleteImage($imageName)
    {
        $pathName = config('image.path') . '/' . $imageName;
        if (!empty($imageName) && file_exists($pathName)
            && $imageName != config('image.imageDefault')
        ) {
            unlink($pathName);
        }
    }

    public function updateImage($request, $currentImage)
    {
        if ($this->verifyImage($request)) {
            $this->deleteImage($currentImage);
            return $this->saveImage($request);
        } else {
            return $currentImage;
        }
    }
}
