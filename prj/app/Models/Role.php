<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    protected $table = 'roles';

    protected $fillable
        = [
            'name',
            'display_name',
        ];

    public function permissions()
    {
        return $this->belongsToMany(
            Permission::class,
            'role_permission',
            'role_id',
            'permission_id'
        );
    }

    public function attachPermision($permissionId)
    {
        return $this->permissions()->attach($permissionId);
    }

    public function dettachPermision()
    {
        return $this->permissions()->detach();
    }

    public function syncPermision($permissionId)
    {
        return $this->permissions()->sync($permissionId);
    }

    public function hasPermission($permission)
    {
        return $this->permissions()->where('name', $permission)->exists();
    }
}
