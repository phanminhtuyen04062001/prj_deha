<?php

namespace App\Models;

use App\Traits\HandleImage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    use HandleImage;

    protected $table = "products";
    protected $fillable = [

            'image',
            'price',
            'describe'
        ];

    public function categories()
    {
        return $this->belongsToMany(
            Category::class,
            'product_category',
            'product_id',
            'category_id'
        );
    }

    public function addCategories($categoryIds)
    {
        return $this->categories()->attach($categoryIds);
    }

    public function syncCategories($categoryIds)
    {
        return $this->categories()->sync($categoryIds);
    }

    public function detachCategory()
    {
        return $this->categories()->detach();
    }

    public function getCategoryName()
    {
        return $this->categories->pluck('name');
    }

    public function scopeWithName($query, $name)
    {
        return $name ? $query->where('name', 'like', '%' . $name . '%') : null;
    }

    public function scopeWithCategoryId($query, $id)
    {
        return $id ? $query->WhereHas(
            'categories',
            function ($query) use ($id) {
                $query->where('id', $id);
            }
        ) : null;
    }

    public function scopeWithCategory($query, $categoryId)
    {
        return $categoryId ? $query->whereHas(
            'categories',
            function ($query) use ($categoryId) {
                $query->where('id', $categoryId);
            }
        ) : null;
    }

    public function scopeWithMinPrice($query, $minPrice)
    {
        return $minPrice ? $query->where('price', '>', $minPrice) : null;
    }

    public function scopeWithMaxPrice($query, $maxPrice)
    {
        return $maxPrice ? $query->where('price', '<', $maxPrice) : null;
    }

    public function getImageAttribute($image)
    {
        return $image ? asset(config('image.path'). '/' .$image) : asset(config('image.imageDefault'));
    }
}
