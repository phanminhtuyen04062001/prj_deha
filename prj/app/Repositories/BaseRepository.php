<?php

namespace App\Repositories;

use Illuminate\Contracts\Container\BindingResolutionException;

abstract class BaseRepository
{
    public $model;

    /**
     * @throws BindingResolutionException
     */
    public function __construct()
    {
        $this->makeModel();
    }

    /**
     * @throws BindingResolutionException
     */
    public function makeModel()
    {
        $this->model = app($this->model());
    }

    abstract public function model();

    public function latest($id)
    {
        return $this->model->latest('id');
    }


    public function all()
    {
        return $this->model->all();
    }

    public function paginate($limit = null, $columns = ['*'])
    {
        $limit = is_null($limit) ? config('repository.pagination.limit', 10)
            : $limit;

        return $this->model->paginate($limit, $columns);
    }


    public function findOrFail($id, $columns = ['*'])
    {
        return $this->model->findOrFail($id);
    }

    public function findWithoutRedirect($id, $columns = ['*'])
    {
        return $this->model->find($id, $columns);
    }


    public function findOrFailWithTrashed($id, $columns = ['*'])
    {
        return $this->model->withTrashed()->findOrFail($id);
    }


    public function create(array $request)
    {
        return $this->model->create($request);
    }

    /**
     * Update a entity in repository by id
     *
     * @param array $request
     * @param       $id
     *
     * @return BaseRepository
     */
    public function update(array $request, $id)
    {
        $model = $this->model->findOrFail($id);
        $model->fill($request);
        $model->save();

        return $model;
    }

    /**
     * Delete a entity in repository by id
     *
     * @param $id
     *
     * @return int
     */
    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    public function multipleDelete(array $ids)
    {
        return $this->model->destroy(array_values($ids));
    }


    public function updateOrCreate(array $arrayFind, $arrayCreate = ['*'])
    {
        return $this->model->updateOrCreate($arrayFind, $arrayCreate);
    }

    public function insertMany($data)
    {
        return count($data) > 0 ? $this->model->insert($data) : null;
    }

    public function count()
    {
        return $this->model->count();
    }
}
