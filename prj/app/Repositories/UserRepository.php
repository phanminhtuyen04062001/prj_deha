<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository extends BaseRepository
{
    public function model()
    {
        return User::class;
    }

    public function search(array $data)
    {
        return $this->model->withName($data['name'] ?? null)
            ->withRoleId($data['role_id'] ?? null)
            ->paginate($data['number_hienthi'] ?? 5);
    }
}
