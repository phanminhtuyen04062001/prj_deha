<?php

namespace App\Repositories;

use App\Models\Category;

class CategoryRepository extends BaseRepository
{
    public function model()
    {
        return Category::class;
    }

    public function search(array $all)
    {
    }
    public function latest($id)
    {
        return $this->model->latest('id');
    }
}
